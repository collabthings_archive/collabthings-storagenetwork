if [ ! -d node_modules/express ]; then npm install express; fi
if [ ! -d node_modules/sleep ]; then npm install sleep; fi
if [ ! -d node_modules/mongodb ]; then npm install mongodb; fi
if [ ! -d node_modules/async ]; then npm install async; fi

pkill pm2

pm2 stop all
sleep 1
pm2 kill
sleep 1

pm2 update

pm2 -i 0 start --no-daemon -i 1 --watch . --name=my-process ecosystem.config.js
pm2 log
