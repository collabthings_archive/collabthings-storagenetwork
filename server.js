'use strict';

var express 		= require('express'), serveStatic = require('serve-static'); 
const PORT=8002;

var exp = express();
exp.use(express.static("src"));

exp.listen(PORT);
console.log("Listening to port " + PORT);
