function log(str) {
	console.log(str);
}

function revisedRandId() {
	var sid = "";
	while(sid.length<25) {
		sid += Math.random().toString(36).replace(/[^a-z]+/g, '');
	}
	
    sid = sid.substr(2, 20);
	
    return sid;
}

function MockApp() {
	this.init = function() {
		var fact = new MockConnectionFactory();
	
		for(var i=0; i<10; i++) {
			fact.registerNode(this.newNode());
		}
		
		for(var i=0; i<10; i++) {
			fact.registerNode(this.newNode());
		}
		
	}
	
	this.newNode = function() {
		return new CSNode(revisedRandId());
	}
	
	this.init();
}