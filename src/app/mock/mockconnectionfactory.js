function MockConnectionFactory() {
	var nodes = [];
	
	this.registerNode = function(n){
		log("registerNode " + n.id);
		nodes[n.id] = n;
	};
	
	this.createConnection = function(n, destid) {
		return new function MockConnection() {
			this.sendMessage = function(m) {
				setTimeout(function() {
					this.nodes[destid].message(m);	
				}, 100);
			};
		}
	}
}
